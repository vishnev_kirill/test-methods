package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Exceptions.SeatsException;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.rmi.server.UID;
import java.util.UUID;

/**
 * Created by Admin on 09.10.2016.
 */
public class SubscriptionTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();



    @Test
    public void testCreateNewSubscriptionWithSmalLeftMinSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(Subscription.SMAL_MINSEATS);
        new Subscription( 10,0,2);
    }

    @Test
    public void testCreateNewSubscriptionWithGrLeftMinSeats()  {
        new Subscription(10,1,2);
    }

    @Test
    public void testCreateNewSubsriptionWithbigLeftMinSeats()  {
        new Subscription(10,2,2);
    }
    @Test
    public void testCreateNewSubsriptionWithSmalRightMinSeats()  {
        new Subscription(999999,999998,2);
    }
    @Test
    public void testCreateNewSubsriptionWithGrRightMinSeats()  {
        new Subscription(999999,999999,2);
    }

    @Test
    public void testCreateNewSubscriptionWithBigRightMinSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(Subscription.BIG_MINSEATS);
        new Subscription(10,1000000,2);
    }



    @Test
    public void testCreateNewSubscriptionWithSmalLeftMaxSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(Subscription.SMAL_MAXSEATS);
        new Subscription( 0,4,2);
    }

    @Test
    public void testCreateNewSubscriptionWithGrLeftMaxSeats()  {
        new Subscription(1,1,2);
    }

    @Test
    public void testCreateNewSubsriptionWithbigLeftMaxSeats()  {
        new Subscription(2,2,2);
    }
    @Test
    public void testCreateNewSubsriptionWithSmalRightMaxSeats()  {
        new Subscription(999998,15,2);
    }
    @Test
    public void testCreateNewSubsriptionWithGrRightMaxSeats()  {
        new Subscription(999999,15,2);
    }

    @Test
    public void testCreateNewSubscriptionWithBigRightMaxSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(Subscription.BIG_MAXSEATS);
        new Subscription(1000000,15,2);
    }


    @Test
    public void testCreateSubscriptionWithUncorrectSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(Subscription.NOT_CORRECT_SEATS);
        new Subscription(20,30,2);
    }

}
