package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Exceptions.*;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;

/**
 * Created by kirill on 27.09.2016.
 */
public class ServicePlanTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewServicePlan()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 5, 8);
    }



    @Test
    public void testCreateNewServicePlanWithShortName()  {
        expectedEx.expect(InvalidServiceNameException.class);
        expectedEx.expectMessage(ServicePlan.UNCORECT_LEN_Of_NAME);
        new ServicePlan("C", "The Best Plan of the year", 10, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithLongName()  {
        StringBuilder b= new StringBuilder();
        for(int i=0; i<=1030; i++){
            b.append("A");
        }

        expectedEx.expect(InvalidServiceNameException.class);
        expectedEx.expectMessage(ServicePlan.UNCORECT_LEN_Of_NAME);
        new ServicePlan(b.toString(), "The Best Plan of the year", 10, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithIncorrectName()  {
        expectedEx.expect(InvalidServiceNameException.class);
        expectedEx.expectMessage(ServicePlan.UNCORECT_FORMAT_Of_NAME);
        new ServicePlan("Comfort+", "The Best Plan of the year", 10, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithNullName()  {
        expectedEx.expect(InvalidServiceNameException.class);
        expectedEx.expectMessage(ServicePlan.NULL_NAME);
        new ServicePlan(null, "The Best Plan of the year", 10, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithShortDetails()  {
        expectedEx.expect(InvalidDetailsException.class);
        expectedEx.expectMessage(ServicePlan.UNCORECT_LEN_Of_DETAILS);
        new ServicePlan("Comfort", "", 10, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithLongDetails()  {
        StringBuilder b= new StringBuilder();
        for(int i=0; i<=1030; i++){
            b.append("A");
        }

        expectedEx.expect(InvalidDetailsException.class);
        expectedEx.expectMessage(ServicePlan.UNCORECT_LEN_Of_DETAILS);
        new ServicePlan("Comfort", b.toString(), 10, 5, 8);
    }


    @Test
    public void testCreateNewServicePlanWithNullDetails()  {
        expectedEx.expect(InvalidDetailsException.class);
        expectedEx.expectMessage(ServicePlan.NULL_DETAILS);
        new ServicePlan("Comfort", null, 10, 5, 8);
    }


    @Test
    public void testCreateNewServicePlanWithSmalLeftMinSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(ServicePlan.SMAL_MINSEATS);
        new ServicePlan("Comfort", "The Best Plan of the year", 5, 0, 8);
    }

    @Test
    public void testCreateNewServicePlanWithGrLeftMinSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 5, 1, 8);
    }

    @Test
    public void testCreateNewServicePlanWithbigLeftMinSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 5, 2, 8);
    }
    @Test
    public void testCreateNewServicePlanWithSmalRightMinSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 999999, 999998, 8);
    }
    @Test
    public void testCreateNewServicePlanWithGrRightMinSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 999999, 999999, 8);
    }

    @Test
    public void testCreateNewServicePlanWithBigRightMinSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(ServicePlan.BIG_MINSEATS);
        new ServicePlan("Comfort", "The Best Plan of the year", 5, 1000000, 8);
    }



    @Test
    public void testCreateNewServicePlanWithSmalLeftMaxSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(ServicePlan.SMAL_MAXSEATS);
        new ServicePlan("Comfort", "The Best Plan of the year", 0, 5, 8);
    }

    @Test
    public void testCreateNewServicePlanWithGrLeftMaxSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 1, 1, 8);
    }

    @Test
    public void testCreateNewServicePlanWithbigLeftMaxSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 2, 2, 8);
    }
    @Test
    public void testCreateNewServicePlanWithSmalRightMaxSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 999998, 3, 8);
    }
    @Test
    public void testCreateNewServicePlanWithGrRightMaxSeats()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 999999, 999999, 8);
    }

    @Test
    public void testCreateNewServicePlanWithBigRightMaxSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(ServicePlan.BIG_MAXSEATS);
        new ServicePlan("Comfort", "The Best Plan of the year", 1000000, 1000000, 8);
    }





    @Test
    public void testCreateNewServicePlanWithUncorrectSeats()  {
        expectedEx.expect(SeatsException.class);
        expectedEx.expectMessage(ServicePlan.NOT_CORRECT_SEATS);
        new ServicePlan("Comfort", "The Best Plan of the year", 4, 10, 8);
    }

    @Test
    public void testCreateNewServicePlanWithSmalLeftFreePerUnit()  {
        expectedEx.expect(InvalidFreePerUnitException.class);
        expectedEx.expectMessage(ServicePlan.SMAL_FREEPERUNIT);
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 5, -1);
    }

    @Test
    public void testCreateNewServicePlanWithGrLeftFreePerUnit()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 1, 0);
    }

    @Test
    public void testCreateNewServicePlanWithbigLeftFreePerUnit()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 2, 1);
    }
    @Test
    public void testCreateNewServicePlanWithSmalRightFreePerUnit()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 3, 999998);
    }
    @Test
    public void testCreateNewServicePlanWithGrRightFreePeraUnir()  {
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 5, 999999);
    }

    @Test
    public void testCreateNewServicePlanWithBigRightFreePerUnit()  {
        expectedEx.expect(InvalidFreePerUnitException.class);
        expectedEx.expectMessage(ServicePlan.BIG_FREEPERUNIT);
        new ServicePlan("Comfort", "The Best Plan of the year", 10, 5, 1000000);
    }
}
