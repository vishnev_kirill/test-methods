package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Exceptions.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() {
        new Customer("John", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }


    /*Имя слишком короткое:
    a.	Создать customer с именем из 1 символа
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithShortFirstName() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_FIRSTNAME);
        new Customer("H", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

   /* Имя слишком длинное:
    a.	Создать customer с именем из 13 символов
    b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewCustomerWithLongFirstName() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_FIRSTNAME);
        new Customer("Thikcjdhxryfj", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

 /* В имени есть пробелы:
    a.	Создать customer с именем, в котором есть пробел
    b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewCustomerWithSpaceFirstName() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_FIRSTNAME);
        new Customer("T dsc", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*Имя не начинается с заглавной буквы
    a.	Создать customer с именем, которое начинается со строчной буквы
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithFirstNameNotCapitalLetter() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_FIRSTNAME);
        new Customer("john", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*В имени есть заглавная буква, которая стоит не в начале
     a.	Создать customer с именем, в котором есть заглавная буква, которая стоит не в начале
     b.	Проверить наличие ошибки и корректного сообщения
 */
    @Test
    public void testCreateNewCustomerWithFirstNameWithCapitalLetter() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_FIRSTNAME);
        new Customer("JoHn", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*В имени есть цифра
    a.	Создать customer с именем, в котором есть цифра
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithFirstNameWithDigit() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_FIRSTNAME);
        new Customer("Jo5n", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }

    /* В имени есть посторонний символ (не буква и не цифра)
       a.	Создать customer с именем, в котором есть посторонний символ
       b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewCustomerWithFirstNameWithSymbol() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_FIRSTNAME);
        new Customer("John!", "Wick", "john_wick@gmail.com", "123qw12U", 0);
    }
    @Test
    public void testCreateNewCustomerWithNullFirstName() {
        expectedEx.expect(InvalidCustomerNameException.class);
        expectedEx.expectMessage(Customer.NULL_FIRSTNAME);
        new Customer(null, "Wick", "john_wick@gmail.com", "123qw12U",0);
    }

    /*Фамилия слишком короткая:
    a.	Создать customer с фамилией из 1 символа
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithShortLastName() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_LASTNAME);
        new Customer("John", "W", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*    Фамилия слишком длинная:
        a.	Создать customer с фамилией из 13 символов
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewCustomerWithLongLastName() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_LASTNAME);
        new Customer("John", "Wdfrilkcbhdrtq", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*    В фамилии есть пробелы:
        a.	Создать customer с фамилией, в которой есть пробел
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewCustomerWithLastNameWithSpace() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_LASTNAME);
        new Customer("John", "W cvc", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*Фамилия не начинается с заглавной буквы
    a.	Создать customer с фамилией, которая начинается со строчной буквы
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithLastNameNotCapitalLetter() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_LASTNAME);
        new Customer("John", "dfvcvc", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*В фамилии есть заглавная буква, которая стоит не в начале
    a.	Создать customer с фамилией, в которой есть заглавная буква, которая стоит не в начале
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithLastNameCapitalLetter() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_LASTNAME);
        new Customer("John", "FfvGcvc", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*
        В фамилии есть цифра
        a.	Создать customer с фамилией, в которой есть цифра
        b.	Проверить наличие ошибки и корректного сообщения
    */
    @Test
    public void testCreateNewCustomerWithLastNameWithDigit() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_LASTNAME);
        new Customer("John", "Wick5", "john_wick@gmail.com", "123qw12U", 0);
    }

    /*В фамилии есть посторонний символ (не буква и не цифра)
    a.	Создать customer с фамилией, в которой есть посторонний символ
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithLastNameWithSymbol() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.INCORRECT_FORMAT_Of_LASTNAME);
        new Customer("John", "Wick!", "john_wick@gmail.com", "123qw12U", 0);
    }

    /* Пароль слишком короткий:
        a.Создать Customer с паролем из 5 символов
        b.Проверить наличие ошибки и корректного сообщения
    */

    @Test
    public void testCreateNewCustomerWithNullLastName() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(Customer.NULL_LASTNAME);
        new Customer("John", null, "john_wick@gmail.com", "123qw12U",0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_PASSWORD);
        new Customer("John", "Wick", "john_wick@gmail.com", "123qw", 0);
    }

    /*
    Пароль слишком длинный:
    a.	Создать customer с паролем из 13 символов
    b.	Проверить наличие ошибки и корректного сообщения
    */
    @Test
    public void testCreateNewCustomerWithLongPass() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LEN_Of_PASSWORD);
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", 0);
    }

    /* Пароль простой
     a.	Создать customer с простым паролем
     b.	Проверить наличие ошибки и корректного сообщения
     */
    //@Test
    //public void testCreateNewCustomerWithEasyPass() {
      //  expectedEx.expect(InvalidPasswordException.class);
        //expectedEx.expectMessage("Password is easy");
        //new Customer("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    //}


    @Test
    public void testCreateNewCustomerWithNullPassword() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.NULL_PASSWORD);
        new Customer("John", "Wick", "john_wick@gmail.com", null,0);
    }

    /*Пароль содержит имя (firstName)
    a.	Создать customer с паролем, содержащим firstName
    b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewCustomerWithPassWithFirstName() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.INCORRECT_PASSWORD);
        new Customer("John", "Wick", "john_wick@gmail.com", "Johnsdd", 0);
    }

    /*Пароль содержит фамилию (lastName)
    a.	Создать customer с паролем, содержащим lastName
    b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewCustomerWithPassWithLastName() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.INCORRECT_PASSWORD);
        new Customer("John", "Wick", "john_wick@gmail.com", "fWicksd", 0);
    }

    /*    Пароль содержит login:
        a.	Создать customer с паролем, содержащим login
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewCustomerWithPassWithLogin() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(Customer.INCORRECT_PASSWORD);
        new Customer("John", "Wick", "jw@mail.ru", "jw@mail.rug", 0);
    }

    /*Cчет отрицательный
    a.	Создать customer с отрицательным счетом
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithNegativeMoney() {
        expectedEx.expect(MoneyException.class);
        expectedEx.expectMessage(Customer.NEGATIVE_MONEY);
        new Customer("John", "Wick", "jw@mail.ru", "123qweDf", -1);
    }

    /*Логин не является e-mail’ом
    a.	Создать customer с логином, который не является e-mail’ом
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewCustomerWithIncorrectLogin() {
        expectedEx.expect(InvalidLoginException.class);
        expectedEx.expectMessage(Customer.INCORRECT_LOGIN);
        new Customer("John", "Wick", "jwmail.ru", "123qweDf", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullLogin() {
        expectedEx.expect(InvalidLoginException.class);
        expectedEx.expectMessage(Customer.NULL_LOGIN);
        new Customer("John", "Wick", null, "123qw12U",0);
    }

}
