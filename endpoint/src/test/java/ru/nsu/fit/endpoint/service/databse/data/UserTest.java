package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.data.Exceptions.*;

/**
 * Created by kirill on 27.09.2016.
 */
public class UserTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewUser() {
        new User("John", "Wick", "john_wick@gmail.com", "strongpass");
    }


    /*Имя слишком короткое:
    a.	Создать User с именем из 1 символа
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithShortFirstName() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_FIRSTNAME);
        new User("H", "Wick", "john_wick@gmail.com", "123qw12U");
    }

   /* Имя слишком длинное:
    a.	Создать User с именем из 13 символов
    b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewUserWithLongFirstName() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_FIRSTNAME);
        new User("Thikcjdhxryfj", "Wick", "john_wick@gmail.com", "123qw12U");
    }

 /* В имени есть пробелы:
    a.	Создать User с именем, в котором есть пробел
    b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewUserWithSpaceFirstName() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_FIRSTNAME);
        new User("T dsc", "Wick", "john_wick@gmail.com", "123qw12U");
    }

    /*Имя не начинается с заглавной буквы
    a.	Создать User с именем, которое начинается со строчной буквы
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithFirstNameNotCapitalLetter() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_FIRSTNAME);
        new User("john", "Wick", "john_wick@gmail.com", "123qw12U");
    }

    /*В имени есть заглавная буква, которая стоит не в начале
     a.	Создать User с именем, в котором есть заглавная буква, которая стоит не в начале
     b.	Проверить наличие ошибки и корректного сообщения
 */
    @Test
    public void testCreateNewUserWithFirstNameWithCapitalLetter() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_FIRSTNAME);
        new User("JoHn", "Wick", "john_wick@gmail.com", "123qw12U");
    }

    /*В имени есть цифра
    a.	Создать User с именем, в котором есть цифра
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithFirstNameWithDigit() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_FIRSTNAME);
        new User("Jo5n", "Wick", "john_wick@gmail.com", "123qw12U");
    }


    @Test
    public void testCreateNewUserWithNullFirstName() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.NULL_FIRSTNAME);
        new User(null, "Wick", "john_wick@gmail.com", "123qw12U");
    }

    /* В имени есть посторонний символ (не буква и не цифра)
       a.	Создать User с именем, в котором есть посторонний символ
       b.	Проверить наличие ошибки и корректного сообщения*/

    @Test
    public void testCreateNewUserWithFirstNameWithSymbol() {
        expectedEx.expect(InvalidUserNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_FIRSTNAME);
        new User("John!", "Wick", "john_wick@gmail.com", "123qw12U");
    }

    /*Фамилия слишком короткая:
    a.	Создать User с фамилией из 1 символа
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithShortLastName() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_LASTNAME);
        new User("John", "W", "john_wick@gmail.com", "123qw12U");
    }

    /*    Фамилия слишком длинная:
        a.	Создать User с фамилией из 13 символов
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewUserWithLongLastName() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_LASTNAME);
        new User("John", "Wdfrilkcbhdrtq", "john_wick@gmail.com", "123qw12U");
    }

    /*    В фамилии есть пробелы:
        a.	Создать User с фамилией, в которой есть пробел
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewUserWithLastNameWithSpace() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_LASTNAME);
        new User("John", "W cvc", "john_wick@gmail.com", "123qw12U");
    }

    /*Фамилия не начинается с заглавной буквы
    a.	Создать User с фамилией, которая начинается со строчной буквы
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithLastNameNotCapitalLetter() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_LASTNAME);
        new User("John", "dfvcvc", "john_wick@gmail.com", "123qw12U");
    }

    /*В фамилии есть заглавная буква, которая стоит не в начале
    a.	Создать User с фамилией, в которой есть заглавная буква, которая стоит не в начале
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithLastNameCapitalLetter() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_LASTNAME);
        new User("John", "FfvGcvc", "john_wick@gmail.com", "123qw12U");
    }

    /*
        В фамилии есть цифра
        a.	Создать User с фамилией, в которой есть цифра
        b.	Проверить наличие ошибки и корректного сообщения
    */
    @Test
    public void testCreateNewUserWithLastNameWithDigit() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_LASTNAME);
        new User("John", "Wick5", "john_wick@gmail.com", "123qw12U");
    }

    /*В фамилии есть посторонний символ (не буква и не цифра)
    a.	Создать User с фамилией, в которой есть посторонний символ
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithLastNameWithSymbol() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.INCORRECT_FORMAT_Of_LASTNAME);
        new User("John", "Wick!", "john_wick@gmail.com", "123qw12U");
    }

    @Test
    public void testCreateNewUserWithNullLastName() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(User.NULL_LASTNAME);
        new User("John", null, "john_wick@gmail.com", "123qw12U");
    }

    /* Пароль слишком короткий:
        a.Создать User с паролем из 5 символов
        b.Проверить наличие ошибки и корректного сообщения
    */
    @Test
    public void testCreateNewUserWithShortPass() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_PASSWORD);
        new User("John", "Wick", "john_wick@gmail.com", "123qw");
    }

    /*
    Пароль слишком длинный:
    a.	Создать User с паролем из 13 символов
    b.	Проверить наличие ошибки и корректного сообщения
    */
    @Test
    public void testCreateNewUserWithLongPass() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.INCORRECT_LEN_Of_PASSWORD);
        new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1");
    }

    /* Пароль простой
     a.	Создать User с простым паролем
     b.	Проверить наличие ошибки и корректного сообщения
     */
    //@Test
   // public void testCreateNewUserWithEasyPass() {
     //   expectedEx.expect(InvalidPasswordException.class);
       // expectedEx.expectMessage("Password is easy");
        //new User("John", "Wick", "john_wick@gmail.com", "123qwe");
    //}

    @Test
    public void testCreateNewUserWithNullPassword() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.NULL_PASSWORD);
        new User("John", "Wick", "john_vick@gmail.com", null);
    }

    /*Пароль содержит имя (firstName)
    a.	Создать User с паролем, содержащим firstName
    b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewUserWithPassWithFirstName() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.INCORRECT_PASSWORD);
        new User("John", "Wick", "john_wick@gmail.com", "Johnsdd");
    }

    /*Пароль содержит фамилию (lastName)
    a.	Создать User с паролем, содержащим lastName
    b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewUserWithPassWithLastName() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.INCORRECT_PASSWORD);
        new User("John", "Wick", "john_wick@gmail.com", "fWicksd");
    }

    /*    Пароль содержит login:
        a.	Создать User с паролем, содержащим login
        b.	Проверить наличие ошибки и корректного сообщения*/
    @Test
    public void testCreateNewUserWithPassWithLogin() {
        expectedEx.expect(InvalidPasswordException.class);
        expectedEx.expectMessage(User.INCORRECT_PASSWORD);
        new User("John", "Wick", "jw@mail.ru", "jw@mail.rug");
    }


    /*Логин не является e-mail’ом
    a.	Создать User с логином, который не является e-mail’ом
    b.	Проверить наличие ошибки и корректного сообщения
*/
    @Test
    public void testCreateNewUserWithIncorrectLogin() {
        expectedEx.expect(InvalidLoginException.class);
        expectedEx.expectMessage(User.INCORRECT_LOGIN);
        new User("John", "Wick", "jwmail.ru", "123qweDf");
    }

    @Test
    public void testCreateNewUserWithNullLogin() {
        expectedEx.expect(InvalidLoginException.class);
        expectedEx.expectMessage(User.NULL_LOGIN);
        new User("John", "Wick", null, "123qweDf");
    }

}
