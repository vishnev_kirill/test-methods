package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.data.Exceptions.SeatsException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {

    public static final String SMAL_MAXSEATS="max seats  should not be less then 1";
    public static final String BIG_MAXSEATS="max seats  should not be more  999999";
    public static final String SMAL_MINSEATS="min seats  should not be less then 1";
    public static final String BIG_MINSEATS="min seats  should not be more then 999999";
    public static final String NOT_CORRECT_SEATS="maxseats < minseats";



    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;
    private int maxSeats;
    private int minSeats;
    private int usedSeats;

    public Subscription( int maxseats, int minseats, int usedseats){
        validate(maxseats,minseats);
        this.id=UUID.randomUUID();
        this.customerId=UUID.randomUUID();
        this.servicePlanId=UUID.randomUUID();
        this.maxSeats=maxseats;
        this.minSeats=minseats;
        this.usedSeats=usedseats;


    }

    private void validate(int maxSeats, int minSeats){
        if (maxSeats<1)
            throw new SeatsException(SMAL_MAXSEATS);

        if (maxSeats>=1000000)
            throw new SeatsException(BIG_MAXSEATS);

        if (minSeats<1)
            throw new SeatsException(SMAL_MINSEATS);

        if (minSeats>=1000000)
            throw new SeatsException(BIG_MINSEATS);

        if (maxSeats<minSeats)
            throw new SeatsException(NOT_CORRECT_SEATS);


    }

    }


