package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;
import ru.nsu.fit.endpoint.service.database.data.Exceptions.*;


import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer {

    public static final String INCORRECT_LEN_Of_FIRSTNAME="FirstName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_FIRSTNAME="Incorrect format of firstName";
    public static final String NULL_FIRSTNAME="Not a firstName";
    public static final String INCORRECT_LEN_Of_LASTNAME="LastName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_LASTNAME="Incorrect format of lastName";
    public static final String NULL_LASTNAME="Not a lastName";

    public static final String INCORRECT_LOGIN ="incorrect format of login";
    public static final String NULL_LOGIN="Not a login";

    public static final String INCORRECT_LEN_Of_PASSWORD = "Password's length should be more or equal 6 symbols and less or equal 12 symbols";
    public static final String INCORRECT_PASSWORD ="incorrect format of password";
    public static final String NULL_PASSWORD="Not a password";

    public static final String NEGATIVE_MONEY = "Negative amount of money";

    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    /* счет не может быть отрицательным */
    private int money;

    public Customer(String firstName, String lastName, String login, String pass, int money)
    {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.money = money;
        validate(firstName, lastName, login, pass, money);
    }

    private static void validate(String firstName, String lastName, String login, String pass, int money) throws MyExcepton {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateLogin(login);
        validatePasssword(pass,firstName,lastName, login);
        validateMoney(money);
    }
    private static void validateFirstName(String firstName) throws MyExcepton{
        if(firstName!=null){
            if(!(firstName.length()>=2 && firstName.length()<13))
                throw new InvalidCustomerNameException(INCORRECT_LEN_Of_FIRSTNAME);
            if(!firstName.matches("[A-Z][a-z]*")){
                throw new InvalidCustomerNameException(INCORRECT_FORMAT_Of_FIRSTNAME);
            }
        }else
            throw new InvalidCustomerNameException(NULL_FIRSTNAME);

    }

    private static void validateLastName(String lastName) throws MyExcepton{

        if(lastName!=null){
            if(!(lastName.length()>=2 && lastName.length()<13)) {
                throw new InvalidCustomerLastNameException(INCORRECT_LEN_Of_LASTNAME);
            }
            if(!lastName.matches("[A-Z][a-z]*")){
                throw new InvalidCustomerLastNameException(INCORRECT_FORMAT_Of_LASTNAME);
            }
        }else
            throw new InvalidCustomerLastNameException(NULL_LASTNAME);

    }
    private static void validateLogin(String login) throws MyExcepton{
        if(login!=null){
            if(!login.matches("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$")){//regex
                throw new InvalidLoginException(INCORRECT_LOGIN);
            }
        } else
            throw new InvalidLoginException(NULL_LOGIN);

    }
    private static void validatePasssword(String pass,String firstName, String lastName, String login) throws MyExcepton{
        if(pass!=null){
            if(!(pass.length()>=6 && pass.length()<13))
                throw new InvalidPasswordException(INCORRECT_LEN_Of_PASSWORD);
            if((pass.contains(firstName)) || (pass.contains(lastName)) || (pass.contains(login))){
                throw new InvalidPasswordException(INCORRECT_PASSWORD);
            }
        }else
            throw new InvalidPasswordException(NULL_PASSWORD);


    }
    private static void validateMoney(int money) throws MyExcepton{
        if(money < 0)
            throw new MoneyException(NEGATIVE_MONEY);
    }




}
